Tech Challenge App
==================
Example *FLASK* App.

[Project](https://gitlab.com/mxc/tech-challenge)

To contribute, setup your environment
```
virtualenv ~/testenv
source ~/testenv/bin/activate
pip install -r requirements.txt
```

Add your update code/tests; then vaildate
```
python ./setup.py test
```

Run with debugger
```
python ./run.py
```


#### Setup a new App

> Note: For example purposes, the .elasticbeanstalk configuration directory has been included, however should you want to start from scratch these would be the steps.  Requires you've configured your AWS creds in ~/.aws/credentials; and that you've installed the awsebcli via `pip install awsebcli`

```
eb init -p Docker -r us-east-1
eb deploy production
```

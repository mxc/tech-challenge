from flask import render_template
from webui import webui
from ._version import __version__

@webui.route('/')
def index():
    return render_template('index.html', version=__version__)

@webui.errorhandler(404)
def page_not_found(e):
    return render_template('404.html',  version=__version__)

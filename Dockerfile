FROM ubuntu:16.04
MAINTAINER ipcrm

COPY conf/nginx.conf /etc/nginx/sites-enabled/localhost.conf
COPY webui/ /usr/src/webui
COPY requirements.txt /usr/src/webui

RUN \
  apt-get update && \
    apt-get install -y nginx supervisor python-pip gunicorn && \
      pip install -r /usr/src/webui/requirements.txt && \
        rm -f /etc/nginx/sites-enabled/default /etc/nginx/sites-available/default

COPY conf/supervisord.conf /etc/supervisor/supervisord.conf

EXPOSE 80
CMD ["/usr/bin/supervisord","-c","/etc/supervisor/supervisord.conf"]


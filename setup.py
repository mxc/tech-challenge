import re
from setuptools import setup

try:
    verstrline = open('webui/_version.py', "rt").read()
except:
    raise RuntimeError("cannot read version file")
else:
    VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
    mo = re.search(VSRE, verstrline, re.M)
    if mo:
        verstr = mo.group(1)
    else:
        raise RuntimeError("unable to find version in yourpackage/_version.py")

setup(
    # Application name:
    name="tech_challenge",

    # Version number (initial):
    version=verstr,

    # Application author details:
    author="mxc",

    # Packages
    packages=["webui"],

    # Include additional files into the package
    include_package_data=True,

    # Details
    url="https://gitlab.com/ipcrm/tech_challenge",

    description="Tech Challenge App",

    # Dependent packages (distributions)
    install_requires=[
        'Flask==0.10.1',
        'Flask-Testing==0.4.2',
        'itsdangerous==0.24',
        'Jinja2==2.8',
        'MarkupSafe==0.23',
        'Werkzeug==0.11.9',
    ],
)
